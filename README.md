# Testing of Reverse Polish Notation #



### What is this repository for? ###
This repository is for writing unit tests for the program 
[**MathExpression**](https://bitbucket.org/MikaelNilsson_Miun/mathexpression) which is an implementation of 
[Reverse Polish Notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation).

The exercise is aimed at using [catch](https://github.com/catchorg/Catch2) to write unit tests for the implementation. 

### How do I get set up? ###

#### Summary of set up ####
Test files are found in `test`folder

##### Setup #####
```
- include (contains program's declaration files)
- src (contains all program source files)
- test (Contains all test files)
- docs/         (The documentation folder with all documentations)
    - Doxyfile  (use doxygen to see documentation)
- CMakeLists.txt
- .gitignore (the .gitignore file)
- README.md (This file)
```

### Who do I talk to? ###
Bernard Che Longho <lobe1602@student.miun.se>