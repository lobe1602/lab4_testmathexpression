/**
 * @author  Mikael Nilsson
 * @file   MathExpression.h
 * @ingroup Definition
 * @brief Class for storing and calculating simple mathematical expressions
 *
*/

#ifndef MATHEXPRESSION_H
#define MATHEXPRESSION_H

#include <string>

/**
 * @class MathExpression
 * @ingroup Defition
 * @brief From a simple linear mathematical expression, convert it to postfix and then calculate the results.
 */

class MathExpression
{
public:
    /**
     * @brief Create a MathExpression with a string
     * @param expression The input string entered by user
     */
    MathExpression(const std::string &expression);

    /**
     * @brief Return a string that is identical to that which was given in the constructor
     * @return string (infixNotation)
     * @test infixNotation is indeed the input string
     */
    std::string infixNotation() const;

    /**
     * @brief from the expression given, extract the postfix expression
     * @return Return the expression in postfix form
     * @test postfix expression must be correctly extracted
     */
    std::string postfixNotation() const;

    /**
     * @brief Calculate the results of the postfix expression
     * @return Calculate the postfix expression and return the value
     * @test value returned must be correct
     */
    double calculate() const;

    /**
     * @return Return true is the input expression entered by the user is in correct format
     * @test If expression is invalid, return true else false
     */
    bool isValid() const;

    /**
     * @return Return an error message if the input expression is in wrong format
     * @test An error message should be printed if a wrong expression(infix) is entered
     * @test No error message printed if infix expression is valid
     */
    std::string errorMessage() const;

    /**
     * @brief Replace the current input expression with expression, setting it to zero.
     */
    MathExpression& operator=(const std::string& expression);
private:
    /* Add private member data and functions */


};
/**
 * @mainpage Unit Testing using catch
 * @brief Writing unit tests for [Math Expression](https://bitbucket.org/MikaelNilsson_Miun/mathexpression)
 * @details This is a laboratory exercise for the course `DT042G Methods and Tools for Software projects`.<BR> This
 * exercise is aimed at using [catch](https://github.com/catchorg/Catch2) to write unit tests for an implementation
 * <BR> of [Reverse Polish Notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation)
 */
 /**
  * @defgroup Definition Definition file for MathExpression
  * @brief Here we have all the functions that are required to be tested.
  * @}
  */
  /**
   * @defgroup Test The tests
   * @brief All tests are included in this group
   * @}
   */
#endif // MATHEXPRESSION_H

