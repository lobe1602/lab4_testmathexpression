/**
 * @author Mikael Nilsson
 * @file CatchMain.cpp
 * @ingroup Test
 * @brief Används för att minska på kompieringstiden genom
 * att definera CATCH_CONFIG_MAIN i en kompileringsenhet
 * som aldrig ändras och därför aldrig behöver kompileras om.
 *
*/

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#ifdef DEBUG
#include <memstat.hpp>
#endif