/**
 * @file test-mathexpression.cpp
 * @ingroup Test
 * @author Bernard Che Longho
 * @brief Writing unit tests for
 * [Math Expression](https://bitbucket.org/MikaelNilsson_Miun/mathexpression)
 * @date 03-Nov-2017
 */

#include <catch.hpp>
#include "../include/MathExpression.h"

/**
 * @brief Test public functions in math expression
 * @ingroup Test
 */
SCENARIO("Evaluating a math expression") {
    GIVEN("A math expression") {
        std::string input;
        MathExpression me(input);

        WHEN("the expression is invalid") {
            std::vector<std::string> expressions;
            expressions.push_back("3++3-3");    // two operators before number
            expressions.push_back("(1+2)(2+4)"); // wrong placement of braces
            expressions.push_back("2+3((34+2)"); // wrong placement of brackets
            expressions.push_back("hello");     // string
            expressions.push_back("1 2 3");     // numbers separated by space not operator
            expressions.push_back("45+");       // operator should separate numbers

            THEN("isValid should be false") {
                for (auto exp: expressions) {
                    input = exp;
                    REQUIRE_FALSE(me.isValid());
                }
            }
            AND_THEN("errorMessage() should print something") {
                for (auto exp: expressions) {
                    input = exp;
                    REQUIRE_FALSE(me.errorMessage().empty());
                }
            }
        }
        AND_WHEN("the expression is valid") {
            std::vector<std::string> validExpressions;
            validExpressions.push_back("2+3");
            validExpressions.push_back("2+4*5");
            validExpressions.push_back("2+3*(23-3)");
            validExpressions.push_back("2+(3-1)-(3+5)");
            validExpressions.push_back("5*(27+3*7)+22");
            validExpressions.push_back("(2)+(3)");
            validExpressions.push_back("2");

            THEN("isValid has to be true") {
                for (auto validExp: validExpressions) {
                    input = validExp;
                    REQUIRE(me.isValid());
                }
            }
            AND_THEN("error message should be empty") {
                for (auto vExp: validExpressions) {
                    input = vExp;
                    REQUIRE(me.errorMessage().empty());
                }
            }
            AND_THEN("infixNotation() should return the expression entered") {
                std::vector<std::string> copyValidExpr{validExpressions};

                for (unsigned int idx = 0; idx < validExpressions.size(); idx++) {
                    input = validExpressions.at(idx);
                    REQUIRE(me.infixNotation() == copyValidExpr.at(idx));
                }
            }
            AND_THEN("postFixNotation() should return the post-fix strings of the expressions") {
                std::vector<std::string> postFixExpressions;
                postFixExpressions.push_back("2 3 +");
                postFixExpressions.push_back("2 4 5 * +");
                postFixExpressions.push_back("2 3 23 3 - * +");
                postFixExpressions.push_back("2 3 1 - + 3 5 + -");
                postFixExpressions.push_back("5 27 3 7 * + * 22 +");
                postFixExpressions.push_back("2 3 +");
                postFixExpressions.push_back("2");

                for (unsigned int i = 0; i < validExpressions.size(); i++) {
                    input = validExpressions.at(i);
                    REQUIRE(me.postfixNotation() == postFixExpressions.at(i));
                }
            }
            AND_THEN("calculate should yield the correct results") {
                std::vector<double> correctResults;
                correctResults.push_back(5);    //  2 3 +
                correctResults.push_back(22);   //  2 4 5 * +
                correctResults.push_back(62);   //  2 3 23 3 - * +
                correctResults.push_back(-4);   //  2 3 1 - + 3 5 + -
                correctResults.push_back(262);  //  5 27 3 7 * + * 22 +
                correctResults.push_back(5);    //  2 3 +
                correctResults.push_back(2);    //  2

                for (unsigned int j = 0; j < validExpressions.size(); j++) {
                    input = validExpressions.at(j);
                    REQUIRE(me.calculate() == correctResults.at(j));
                }
            }
        }
    }
}
